#include <iostream>
#include "abr.cpp"

void disInt(int i) {
    cout << i << " ";
}

int main() {

    /**
    BinarySearchTree<int> tree;
    tree.insert(10);
    tree.insert(7);
    tree.insert(15);
    tree.insert(2);
    tree.insert(8);
    tree.display();

    cout << boolalpha << tree.contains(7) << endl;

    cout << "Min : " << tree.min() << endl;
    tree.deleteMin();
    tree.display();


    tree.deleteElement(10);
    tree.display();**/

    BinarySearchTree<int> tree;
    tree.insert(10);
    tree.insert(12);
    tree.insert(16);
    tree.insert(15);
    tree.insert(7);
    tree.insert(1);
    tree.insert(12);
    tree.insert(5);
    tree.insert(11);
    tree.insert(11);
    tree.insert(4);
    tree.insert(6);
    tree.insert(0);
    tree.insert(13);
    tree.display();

    cout << "Recherche des valeurs de 0 à 16" << endl;
    vector<int> trouve, absent;
    for(int i = 0; i < 17; i++) {
        if(tree.contains(i)) {
            trouve.push_back(i);
        } else {
            absent.push_back(i);
        }
    }
    cout << "Trouves: ";
    for(int i : trouve)
        cout << i << " ";
    cout << "\nAbsents: ";
    for(int i : absent)
        cout << i << " ";

    cout << "\nRecherche du minimum: " << tree.min() << endl;
    cout << "Suppression du minimum (2x) " << endl;
    tree.deleteMin();
    cout << "Etat de l'arbre:" << endl;
    tree.display();
    tree.deleteMin();
    cout << "Etat de l'arbre:" << endl;
    tree.display();

    cout << "Suppression de 10" << endl;
    tree.deleteElement(10);
    tree.display();

    cout << "Suppression de 16" << endl;
    tree.deleteElement(16);
    tree.display();

    cout << "Suppression de 7" << endl;
    tree.deleteElement(7);
    tree.display();

    cout << "Test des parcours..." << endl;
    cout << "Pre-ordonne  : ";tree.visitPre(disInt);
    cout << "\nSymetrique   : ";tree.visitSym(disInt);
    cout << "\nPost-ordonne : ";tree.visitPost(disInt);
    cout << endl;

    cout << "Nombre d'elements : " << tree.size() << endl;

    cout << "Test du rank :" << endl;
    for(size_t i = 0; i < tree.size(); i++) {
        cout << tree.rank(tree.nth_element(i)) << " ";
    }

    cout << "\nTest constructeur deplacement : " << endl;
    BinarySearchTree<int> abr2 = move(tree);

    cout << "\nTest constructeur copie : " << endl;
    BinarySearchTree<int> abr3(abr2);

    BinarySearchTree<int> abr4;
    cout << "\nAffectation par deplacement : " << endl;
    abr4 = std::move(abr2);
    cout << "\nAffectation par copie : " << endl;
    abr4 = abr3;


    BinarySearchTree<int> abr5;
    abr5.insert(3);
    abr5.insert(1);
    abr5.insert(7);
    abr5.insert(2);
    abr5.insert(4);

//    abr5.display();
//    abr5.linearize();
    abr5.display();
    abr5.balance();
    abr5.display();


    return EXIT_SUCCESS;
}