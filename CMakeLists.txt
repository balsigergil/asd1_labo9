cmake_minimum_required(VERSION 3.14)
project(Labo_09)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wconversion -Wextra -pedantic")

add_executable(Labo_09 abr.cpp main.cpp)